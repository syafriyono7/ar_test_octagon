﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class duplicateWord : MonoBehaviour
{
    public Text input;
    private string defaultText = "cabcabbaacbca";
    public List<string> Char = new List<string>();
    public Button btnProses;
    public Text Result;

    void Awake()
    {
        input.text = defaultText;
        btnProses = GetComponent<Button>();
    }

    void Start()
    {
        string words = (string.IsNullOrEmpty(input.text))? defaultText : input.text;
        btnProses.onClick.AddListener(()=> replaceDuplicated(words));
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape)){
            Application.Quit();
        }
    }

    public void replaceDuplicated(string Words)
    {
        foreach(char word in Words){
            string _word = word.ToString();
            if(!Char.Contains(_word)){
                Char.Add(_word);
            }
        }
        Char.Sort();
        string res = string.Join(", ", Char.ToArray());
        Result.text = "Resut : "+res;
    }


}
